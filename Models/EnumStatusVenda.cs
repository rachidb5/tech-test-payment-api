﻿namespace TrilhaApiDesafio.Models
{
    public enum EnumStatusVenda
    {
        Aguardando,
        Aprovado,
        Enviado,
        Entregue,
        Cancelada
    }
}