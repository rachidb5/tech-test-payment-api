﻿using Microsoft.AspNetCore.Mvc;
using TechTestPaymentApi.Context;
using TechTestPaymentApi.Models;
using TrilhaApiDesafio.Models;

namespace TechTestPaymentApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly OrganizadorContext _context;

        public VendaController(OrganizadorContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult CriarVenda(Venda venda)
        {

            _context.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterVendaPorId), new { id = venda.Id }, venda);
        }
        [HttpGet("{id}")]
        public IActionResult ObterVendaPorId(int id)
        {
            var item = _context.Vendas.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return Ok(item);
        }
        [HttpGet]
        public IActionResult ObterTodasAsVendas()
        {
            var vendas = _context.Vendas.ToList();
            if (vendas == null)
            {
                return NotFound();
            }
            return Ok(vendas);
        }
        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, EnumStatusVenda enumStatusVenda)
        {
            var tarefaBanco = _context.Vendas.Find(id);

            if (tarefaBanco == null)
                return NotFound();

            if(tarefaBanco.Status == EnumStatusVenda.Aguardando)
            {
                if(enumStatusVenda != EnumStatusVenda.Aprovado)
                {

                    if (enumStatusVenda != EnumStatusVenda.Cancelada)
                    {
                        return BadRequest("Status Invalido");
                    }
                }
            }

            if (tarefaBanco.Status == EnumStatusVenda.Aprovado)
            {
                if (enumStatusVenda != EnumStatusVenda.Enviado)
                {
                    if (enumStatusVenda != EnumStatusVenda.Cancelada)
                    {
                        return BadRequest("Status Invalido");
                    }
                }
            }

            if (tarefaBanco.Status == EnumStatusVenda.Enviado)
            {
                if (enumStatusVenda != EnumStatusVenda.Entregue)
                {
                    return BadRequest("Status Invalido");
                 
                }
            }
            _context.Update(tarefaBanco);
            _context.SaveChanges();
            return Ok(tarefaBanco);
        }

    }
}
