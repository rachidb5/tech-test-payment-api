﻿using Microsoft.AspNetCore.Mvc;
using TechTestPaymentApi.Context;
using TechTestPaymentApi.Models;

namespace TechTestPaymentApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly OrganizadorContext _context;

        public VendedorController(OrganizadorContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Criar(Vendedor vendedor)
        {

            _context.Add(vendedor);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterVendedorPorId), new { id = vendedor.Id }, vendedor);
        }
        [HttpGet("{id}")]
        public IActionResult ObterVendedorPorId(int id)
        {
            var vendedor = _context.Vendedor.Find(id);
            if (vendedor == null)
            {
                return NotFound();
            }
            return Ok(vendedor);
        }



    }
}
