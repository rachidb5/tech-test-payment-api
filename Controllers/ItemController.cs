﻿using Microsoft.AspNetCore.Mvc;
using TechTestPaymentApi.Context;
using TechTestPaymentApi.Models;

namespace TechTestPaymentApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ItemController : ControllerBase
    {
        private readonly OrganizadorContext _context;

        public ItemController(OrganizadorContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Criar(Item item)
        {

            _context.Add(item);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { id = item.Id }, item);
        }
        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var item = _context.Items.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return Ok(item);
        }



    }
}
