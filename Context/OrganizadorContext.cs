using Microsoft.EntityFrameworkCore;
using TechTestPaymentApi.Models;

namespace TechTestPaymentApi.Context
{
    public class OrganizadorContext : DbContext
    {
        public OrganizadorContext(DbContextOptions<OrganizadorContext> options) : base(options)
        {

        }

        public DbSet<Item> Items { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedor { get; set; }
    }
}